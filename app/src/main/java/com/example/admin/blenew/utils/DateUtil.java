package com.example.admin.blenew.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint({"SimpleDateFormat"})
public class DateUtil {
    public static String getCurrentDatatime() {
        return new SimpleDateFormat("yy-MM-dd HH:mm:ss").format(new Date());
    }

    public static int getYear(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(1);
    }

    public static int getMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(2) + 1;
    }

    public static int getDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(5);
    }

    public static String getMonth(Date date, Locale locale) {
        return new SimpleDateFormat("MMM", locale).format(date);
    }

    public static int getMonthWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(4);
    }

    public static int getYearWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(3);
    }

    public static String toDateYYYYMMDD(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String toDateYYYYMM(Date date) {
        return new SimpleDateFormat("yyyyMM").format(date);
    }

    public static Date dateAddDay(Date date, int addDay) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(6, addDay);
        return c.getTime();
    }

    public static Date dateAddMonth(Date date, int addMonth) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(2, addMonth);
        return c.getTime();
    }

    public static int getCurrentMinutes() {
        Calendar c = Calendar.getInstance();
        return (c.get(11) * 60) + c.get(12);
    }
}

package com.example.admin.blenew.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmiLogResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
}

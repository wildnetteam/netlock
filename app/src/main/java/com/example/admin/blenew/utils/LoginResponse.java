package com.example.admin.blenew.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;


    public class Data {
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("username")
        @Expose
        public String username;
        @SerializedName("user_username")
        @Expose
        public String userUsername;
        @SerializedName("user_address")
        @Expose
        public String userAddress;
        @SerializedName("lockcode")
        @Expose
        public String lockcode;
        @SerializedName("barcode")
        @Expose
        public String barcode;
        @SerializedName("userid")
        @Expose
        public String userid;
        @SerializedName("lock_serial")
        @Expose
        public String lock_serial;
        @SerializedName("role")
        @Expose
        public String role;

    }
}

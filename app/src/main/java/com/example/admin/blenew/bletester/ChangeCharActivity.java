package com.example.admin.blenew.bletester;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.blenew.adapter.BleDeviceListAdapter2;
import com.example.admin.blenew.service.BleService;
import com.example.admin.blenew.utils.AppData;
import java.util.UUID;

public class ChangeCharActivity extends Fragment {
    BleService bleService;
    TextView charHex;
    TextView charString;
    UUID charUuid;
    private MainActivity mcontext;
    private int num=0;
    private ServiceConnection conn = new ServiceConnection() {
        @SuppressLint({"NewApi"})
        public void onServiceConnected(ComponentName arg0, IBinder service) {
            bleService = ((BleService.LocalBinder) service).getService();
            gattChar = bleService.mBluetoothGatt.getService(serUuid).getCharacteristic(charUuid);
            bleService.mBluetoothGatt.readCharacteristic(gattChar);
            if (gattChar.getDescriptors().size() != 0) {
                BluetoothGattDescriptor des = (BluetoothGattDescriptor) gattChar.getDescriptors().get(0);
                    des.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                     bleService.mBluetoothGatt.writeDescriptor(des);
            }
            int prop = gattChar.getProperties();
               if(num ==0) {
                   if ((prop & 8) > 0) {
                       bleService.mBluetoothGatt.setCharacteristicNotification(gattChar, false);
                   }
                   if ((prop & 4) > 0) {
                       bleService.mBluetoothGatt.setCharacteristicNotification(gattChar, false);
                   }
                   if ((prop & 2) > 0) {
                       bleService.mBluetoothGatt.setCharacteristicNotification(gattChar, false);
                   }
                   if ((prop & 16) > 0) {
                       bleService.mBluetoothGatt.setCharacteristicNotification(gattChar, true);
                   }
               }else{
                   bleService.mBluetoothGatt.setCharacteristicNotification(gattChar, true);
               }

        }

        public void onServiceDisconnected(ComponentName arg0) {
            bleService = null;
        }
    };

    Editor editor;
    BluetoothGattCharacteristic gattChar;
    boolean isNotifyHex;
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @SuppressLint({"NewApi", "WrongConstant"})
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if ("com.example.bluetooth.le.ACTION_GATT_RSSI".equals(action)) {
                rssi = intent.getExtras().getInt("com.example.bluetooth.le.ACTION_GATT_RSSI");
                getActivity().invalidateOptionsMenu();
            }
            if (BleService.ACTION_CHAR_READED.equals(action)) {
//                final String des1String = intent.getExtras().getString("desriptor1");
//                final String des2String = intent.getExtras().getString("desriptor2");
//                final String stringValue = intent.getExtras().getString("StringValue");
//                final String hexValue = intent.getExtras().getString("HexValue");
//                final String readTime = intent.getExtras().getString("time");

//                if(num==0) {
//                    num = num++;
//                    if(isAdded() && isVisible()  )
//                    againWriteForUnlock();
//                }
          }
            if (BleService.ACTION_DATA_AVAILABLE.equals(action)) {
                ChangeCharActivity changeCharActivity;
                if (isNotifyHex) {
                    result = intent.getExtras().getString(BleService.EXTRA_DATA);
                } else {
                    result = intent.getExtras().getString(BleService.EXTRA_STRING_DATA);
                }
                int countNumber = intent.getExtras().getInt(BleService.EXTRA_DATA_LENGTH);
                if (resultLengthNum != 0) {
                    changeCharActivity = ChangeCharActivity.this;
                    changeCharActivity.resultLengthNum += countNumber;
                } else {
                    resultLengthNum = countNumber;
                }
                if (text_string != null) {
                    changeCharActivity = ChangeCharActivity.this;
                    changeCharActivity.text_string += result;
                } else {
                    text_string = result;
                }
                resultLength = new StringBuilder(String.valueOf(resultLengthNum)).toString();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        notify_resualt.setText(text_string);
                        resultcount.setText("Number of bytes： " + resultLength);
                    }
                });
            }
            if (BleService.ACTION_GATT_DISCONNECTED.equals(action)) {
                Toast.makeText(getActivity(), "Device disconnected", 0).show();
//                if (sharedPreferences.getBoolean("AutoConnect", true)) {
//                    bleService.connect(DeviceConnect.bleAddress);
//                }
            }
        }
    };

    @SuppressLint("WrongConstant")
    private void againWriteForUnlock() {
        final String transStr = "F56000005FB4";

         Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    getActivity().unbindService(conn);
                    getActivity().bindService(new Intent(getActivity(), BleService.class), conn, 1);
                            try {
                                gattChar.setValue(hexStringToByteArray(transStr));
                                bleService.mBluetoothGatt.writeCharacteristic(gattChar);
                                //CallDestroy();
                                //new BleDeviceListAdapter2().clear();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                }
            }
        };
        thread.start();
    }

    TextView notify_resualt;
    int prop;
    String result = null;
    String resultLength = null;
    int resultLengthNum;
    TextView resultcount;
    int rssi;
    UUID serUuid;
    SharedPreferences sharedPreferences;
    String text_string = null;
    TextView time;
    private BluetoothDevice device = null;

    @SuppressLint("WrongConstant")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mcontext =((MainActivity)getActivity()) ;
        this.sharedPreferences = getActivity().getSharedPreferences("writedata", 0);
        this.editor = this.sharedPreferences.edit();
        charUuid = UUID.fromString(String.valueOf(AppData.getInstance().getCharUUID()));
        serUuid = UUID.fromString(String.valueOf(AppData.getInstance().getUUID()));
        prop = AppData.getInstance().getProperties();
        Log.e("charUuid----", String.valueOf(charUuid));
        Log.e("serUuid----", String.valueOf(serUuid));
        Log.e("prop----", String.valueOf(prop));
        getActivity().bindService(new Intent(getActivity(), BleService.class), conn, 1);
        getActivity().registerReceiver(this.mBroadcastReceiver, makeIntentFilter());
        num=0;
        writeChar();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private IntentFilter makeIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BleService.ACTION_CHAR_READED);
        intentFilter.addAction(BleService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BleService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BleService.BATTERY_LEVEL_AVAILABLE);
        intentFilter.addAction("com.example.bluetooth.le.ACTION_GATT_RSSI");
        return intentFilter;
    }

    public void onDestroy() {
        super.onDestroy();
          CallDestroy();
    }

    private void CallDestroy() {
        getActivity().unbindService(conn);
        getActivity().unregisterReceiver(this.mBroadcastReceiver);
    }

    private void writeChar() {
        final String transStr =  AppData.getInstance().getLockCode();
         Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                            try {
                                gattChar.setValue(str2Byte(transStr));
                                boolean status =   bleService.mBluetoothGatt.writeCharacteristic(gattChar);
//                                CallDestroy();
//                                new BleDeviceListAdapter2().clear();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                }
            }
        };
        thread.start();
    }

    private void FinishServices() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        CallDestroy();
                        new MainActivity().CallDestroy();
                        BleService bleService = new BleService();
                        bleService.mBluetoothGatt.disconnect();
                        bleService.mBluetoothGatt.close();
                        if (bleService.mBluetoothAdapter != null) {
                            bleService.mBluetoothAdapter.cancelDiscovery();
                            bleService.mBluetoothAdapter = null;
                        }
                        new BleDeviceListAdapter2().clear();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public static String str2HexStr(String str) {
        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        for (int i = 0; i < bs.length; i++) {
            sb.append(chars[(bs[i] & 240) >> 4]);
            sb.append(chars[bs[i] & 15]);
        }
        return sb.toString().trim();
    }

    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[(hexStr.length() / 2)];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) (((str.indexOf(hexs[i * 2]) * 16) + str.indexOf(hexs[(i * 2) + 1])) & 255);
        }
        return new String(bytes);
    }

    public static byte[] str2Byte(String hexStr) {
        int i;
        if (hexStr.length() % 2 != 0) {
            hexStr = "0" + hexStr;
        }
        String[] a = new String[(hexStr.length() / 2)];
        byte[] bytes = new byte[(hexStr.length() / 2)];
        for (i = 0; i < bytes.length; i++) {
            a[i] = hexStr.substring(i * 2, (i * 2) + 2);
        }
        for (i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(a[i], 16);
        }
        return bytes;
    }

    public static byte[] hexStringToByteArray(String s) {
        if (s.length() % 2 != 0) {
            StringBuilder stringBuilder = new StringBuilder(s);
            stringBuilder.insert(s.length() - 1, "0");
            s = stringBuilder.toString();
        }


        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }



}

package com.example.admin.blenew.utils;

import java.util.UUID;

public class AppData {
    public static final AppData ourInstance = new AppData();
    private String qrResult = null;
    private String deviceName;
    private String deviceAddress;
    private String lockCode;
    private UUID charUUID;
    private int properties;
    private UUID UUID;
    private String lockName;
    private String userId;
    private String RoleId;

    public static AppData getInstance() {
        return ourInstance;
    }

    public AppData() {
    }

    public void setQrResult(String qrResult) {
        this.qrResult = qrResult;
    }

    public String getQrResult() {
        return qrResult;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setLockCode(String lockCode) {
        this.lockCode = lockCode;
    }

    public String getLockCode() {
        return lockCode;
    }

    public void setCharUUID(UUID charUUID) {
        this.charUUID = charUUID;
    }

    public UUID getCharUUID() {
        return charUUID;
    }

    public void setProperties(int properties) {
        this.properties = properties;
    }

    public int getProperties() {
        return properties;
    }

    public void setUUID(UUID uuid) {
        this.UUID = uuid;
    }

    public UUID getUUID() {
        return UUID;
    }

    public void setLockName(String lockName) {
        this.lockName = lockName;
    }

    public String getLockName() {
        return lockName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return RoleId;
    }

    public void setRoleId(String roleId) {
        RoleId = roleId;
    }
}

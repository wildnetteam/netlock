package com.example.admin.blenew.bletester;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.blenew.R;
import com.example.admin.blenew.service.BleService;
import com.example.admin.blenew.utils.AppData;
import com.example.admin.blenew.utils.ProgressD;
import com.example.admin.blenew.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@SuppressLint("ValidFragment")
public class DeviceConnect extends Fragment {
    public static final String CANCEL_DEVICE_ALARM = "find.device.cancel.alarm";
    public static final String DEVICE_BATTERY = "device.battery.level";
    public static final String DISCONNECT_DEVICE = "find.device.disconnect";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String FIND_DEVICE_ALARM_ON = "find.device.alarm.on";
    public static String bleAddress;
    private ProgressD progress;
    BleService bleService;
    public static DeviceConnect deviceConnect;
    private final ServiceConnection conn = new ServiceConnection() {
        @SuppressLint({"NewApi"})
        public void onServiceConnected(ComponentName name, IBinder service) {
            bleService = ((BleService.LocalBinder) service).getService();
            if (!bleService.init()) {
                Log.e("DEVICE---", "NOT CONNECTED");
            }
            bleService.connect(DeviceConnect.bleAddress);
            Log.d("DEVICE---", "CONNECTED");
        }

        public void onServiceDisconnected(ComponentName name) {
            bleService = null;
        }
    };
    Editor editor;
    List<BluetoothGattService> gattServices = new ArrayList();


    BroadcastReceiver mbtBroadcastReceiver = new BroadcastReceiver() {
        @SuppressLint({"NewApi", "DefaultLocale", "WrongConstant"})
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BleService.ACTION_GATT_CONNECTED.equals(action)) {
              if (progress != null)
                progress.dismiss();
                // Toast.makeText(getActivity(), "Device connection succeeded！", 1).show();

            }

            if (BleService.ACTION_GATT_DISCONNECTED.equals(action)) {
                progress.dismiss();
                //  Toast.makeText(getActivity(), "Device disconnected！", 1).show();
               /* if (sharedPreferences.getBoolean("AutoConnect", true)) {
                    bleService.connect(bleAddress);
                    progress.dismiss();
                }*/
            }

            if (BleService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                progress.dismiss();
                bleService.mBluetoothGatt.readRemoteRssi();
                gattServices = bleService.mBluetoothGatt.getServices();

                final ArrayList<HashMap<String, String>> serviceNames = new ArrayList();

                for (BluetoothGattService ser : gattServices) {
                    Object obj;
                    HashMap<String, String> currentServiceData = new HashMap();
                    String uuid = ser.getUuid().toString();
                    String str = "Name";
                    if (Utils.attributes.containsKey(uuid)) {
                        obj = (String) Utils.attributes.get(uuid);
                    } else {
                        obj = "Unknown Service";
                    }
                    currentServiceData.put(str, String.valueOf(obj));
                    serviceNames.add(currentServiceData);
                }

                getActivity().runOnUiThread(new Runnable() {
                    @SuppressLint({"NewApi"})
                    public void run() {
                        try {
                            UUID serviceUUID = ((BluetoothGattService) bleService.mBluetoothGatt.getServices().get(3)).getUuid();
                            AppData.getInstance().setCharUUID(((BluetoothGattCharacteristic) bleService.mBluetoothGatt.getService(serviceUUID).getCharacteristics().get(1)).getUuid());
                            AppData.getInstance().setProperties(((BluetoothGattCharacteristic) bleService.mBluetoothGatt.getService(serviceUUID).getCharacteristics().get(1)).getProperties());
                            AppData.getInstance().setUUID(serviceUUID);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Fragment newFragment = new ChangeCharActivity();
                        android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.device_lauout, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });
            }
            if ("com.example.bluetooth.le.ACTION_GATT_RSSI".equals(action)) {
                rssi = intent.getExtras().getInt("com.example.bluetooth.le.ACTION_GATT_RSSI");
                getActivity().invalidateOptionsMenu();
            }
        }
    };
    int rssi;
    SharedPreferences sharedPreferences;


    public DeviceConnect(ProgressD progressD) {
        this.progress = progressD;
    }

    public DeviceConnect() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        deviceConnect = this;
        sharedPreferences = getActivity().getPreferences(0);
        editor = this.sharedPreferences.edit();
        bleAddress = AppData.getInstance().getDeviceAddress();
        bindBleSevice();
        getActivity().registerReceiver(mbtBroadcastReceiver, makeGattUpdateIntentFilter());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @SuppressLint("WrongConstant")
    private void bindBleSevice() {
        getActivity().bindService(new Intent(getActivity(), BleService.class), this.conn, 1);
    }

    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
        CallFinish();
    }

    public void CallFinish() {
        getActivity().unbindService(conn);
        getActivity().unregisterReceiver(mbtBroadcastReceiver);
    }


    private static IntentFilter makeGattUpdateIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BleService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BleService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BleService.BATTERY_LEVEL_AVAILABLE);
        intentFilter.addAction("com.example.bluetooth.le.ACTION_GATT_RSSI");
        return intentFilter;
    }

}

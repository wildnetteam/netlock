package com.example.admin.blenew.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.admin.blenew.R;
import com.example.admin.blenew.adapter.BleDeviceListAdapter2;
import com.example.admin.blenew.bletester.DeviceConnect;
import com.example.admin.blenew.bletester.MainActivity;
import com.example.admin.blenew.utils.ApiClient;
import com.example.admin.blenew.utils.ApiInterface;
import com.example.admin.blenew.utils.AppData;
import com.example.admin.blenew.utils.Constants;
import com.example.admin.blenew.utils.DateUtil;
import com.example.admin.blenew.utils.LoginPreferences;
import com.example.admin.blenew.utils.LoginResponse;
import com.example.admin.blenew.utils.ProgressD;
import com.example.admin.blenew.utils.SubmiLogResponse;
import com.example.admin.blenew.utils.Utils;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BleService extends Service {
    public static final String ACTION_CHAR_READED = "com.example.bluetooth.le.ACTION_CHAR_READED";
    public static final String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public static final String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_RSSI = "com.example.bluetooth.le.ACTION_GATT_RSSI";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public static final UUID BATTERY_CHAR_UUID = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    public static final String BATTERY_LEVEL_AVAILABLE = "com.example.bluetooth.le.BATTERY_LEVEL_AVAILABLE";
    public static final UUID BATTERY_SERVICE_UUID = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
    public static final UUID C22D = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
    public static final String EXTRA_DATA_LENGTH = "com.example.bluetooth.le.EXTRA_DATA_LENGTH";
    public static final String EXTRA_DATA_RSSI = "com.example.bluetooth.le.ACTION_GATT_RSSI";
    public static final String EXTRA_STRING_DATA = "com.example.bluetooth.le.EXTRA_STRING_DATA";
    public static final UUID MY_CHAR_UUID = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb");
    public static final UUID MY_SERVICE_UUID = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_ALART_UUID = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_CHAR_UUID = UUID.fromString("00002A06-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_SERVICE_UUID = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_DISCONNECTED = 0;
    private static final String TAG = "BleService";
    public static final UUID TX_CHAR_UUID = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
    private final IBinder mBinder = new LocalBinder();
    public BluetoothAdapter mBluetoothAdapter;
    public BluetoothGatt mBluetoothGatt;

    @SuppressLint({"NewApi"})
    public BluetoothGattCallback mBluetoothGattCallback = new BluetoothGattCallback() {
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == 2) {
                intentAction = BleService.ACTION_GATT_CONNECTED;
                mConnectionState = 2;
                broadcastUpdate(intentAction);
                gatt.discoverServices();
            } else if (newState == 0) {
                intentAction = BleService.ACTION_GATT_DISCONNECTED;
                mConnectionState = 0;
                broadcastUpdate(intentAction);
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == 0) {
                broadcastUpdate(BleService.ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w("mylog", "service is null");
            }
        }


        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.e("Akash", "Write "+Utils.bytesToHex(characteristic.getValue()));
            //  super.onCharacteristicWrite(gatt, characteristic, status);
        }




        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            Log.e("Akash", "Read "+Utils.bytesToHex(characteristic.getValue()));
            if (status == BluetoothGatt.GATT_SUCCESS) {
                              //  getChartacteristicValue(characteristic);
                if(MainActivity.isScanButtonClick){
                    final Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                try {

                                    disconnect();

                                    MainActivity.activity.finishAffinity();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    };
                    thread.start();


                }else {
                    MainActivity.callSubmitLogApi(LoginPreferences.getActiveInstance(getApplicationContext()).getRole(), "1", "0");
                    FinishServices(3000);

                }

            } else {
                Log.e(BleService.TAG, " BluetoothGatt Read Failed!");
            }
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.e("Akash", "Change "+Utils.bytesToHex(characteristic.getValue()));
            broadcastUpdate(BleService.ACTION_DATA_AVAILABLE, characteristic);
        }



        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Intent rssiIntent = new Intent();
            rssiIntent.putExtra("com.example.bluetooth.le.ACTION_GATT_RSSI", rssi);
            rssiIntent.setAction("com.example.bluetooth.le.ACTION_GATT_RSSI");
            sendBroadcast(rssiIntent);
            if (mBluetoothGatt != null) {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(1500);
                            mBluetoothGatt.readRemoteRssi();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        }
    };

    public  void FinishServices(final int time) {



        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {

                        disconnect();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public BluetoothManager mBluetoothManager;
    public int mConnectionState = 0;
    private String mbluetoothDeviceAddress;
    public String notify_result;
    public int notify_result_length;
    public String notify_string_result;

    public class LocalBinder extends Binder {
        public BleService getService() {
            return BleService.this;
        }
    }

    @SuppressLint({"NewApi"})
    private void getChartacteristicValue(BluetoothGattCharacteristic characteristic) {
        List<BluetoothGattDescriptor> des = characteristic.getDescriptors();
        Intent mIntent = new Intent(ACTION_CHAR_READED);
        if (des.size() != 0) {
            mIntent.putExtra("desriptor1", des.get(0).getUuid().toString());
            /* mIntent.putExtra("desriptor2", des.get(1).getUuid().toString());*/
        }
        mIntent.putExtra("StringValue", characteristic.getStringValue(0));
        // Battery level read value
        if (UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb")
                .equals(characteristic.getUuid())) {
            mIntent.putExtra("HexValue", Utils.getBatteryLevel(characteristic));

        }else {
            mIntent.putExtra("HexValue", Utils.bytesToHex(characteristic.getValue()));
        }
        Log.e("Volt", ""+ Utils.bytesToHex(characteristic.getValue()));
        mIntent.putExtra("time", DateUtil.getCurrentDatatime());
        sendBroadcast(mIntent);
    }


    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void broadcastUpdate(String action) {
        sendBroadcast(new Intent(action));
    }

    @SuppressLint({"NewApi"})
    private void broadcastUpdate(String action, BluetoothGattCharacteristic characteristic) {
        Intent intent = new Intent();
        intent.setAction(action);
        byte[] data = characteristic.getValue();
        String stringData = characteristic.getStringValue(0);
        if (data != null && data.length > 0) {
            StringBuilder stringBuilder = new StringBuilder(data.length);
            int length = data.length;
            for (int i = 0; i < length; i++) {
                stringBuilder.append(String.format("%X", Byte.valueOf(data[i])));
            }
            if (stringData != null) {
                intent.putExtra(EXTRA_STRING_DATA, stringData);
            } else {
                Log.v("tag", "characteristic.getStringValue is null");
            }
            notify_result = stringBuilder.toString();
            notify_string_result = stringData;
            notify_result_length = data.length;
            intent.putExtra(EXTRA_DATA, notify_result);
            intent.putExtra(EXTRA_DATA_LENGTH, notify_result_length);
        }
        sendBroadcast(intent);
    }

    @SuppressLint({"NewApi", "WrongConstant"})
    public boolean init() {
        IntentFilter bleSeviceFilter = new IntentFilter();
        bleSeviceFilter.addAction(DeviceConnect.FIND_DEVICE_ALARM_ON);
        bleSeviceFilter.addAction(DeviceConnect.CANCEL_DEVICE_ALARM);
        bleSeviceFilter.addAction(DeviceConnect.DEVICE_BATTERY);
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService("bluetooth");
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
            mBluetoothAdapter = mBluetoothManager.getAdapter();
        }
        if (mBluetoothAdapter != null) {
            return true;
        }
        Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
        return false;
    }

    @SuppressLint({"NewApi"})
    public boolean connect(String bleAddress) {
        if (mBluetoothAdapter == null || bleAddress == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        } else if (mbluetoothDeviceAddress == null || !bleAddress.equals(mbluetoothDeviceAddress) || mBluetoothGatt == null) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bleAddress);
            if (device == null) {
                Log.w(TAG, "Device not found.  Unable to connect.");
                return false;
            }
            mBluetoothGatt = device.connectGatt(this, false, mBluetoothGattCallback);
            mbluetoothDeviceAddress = bleAddress;
            mConnectionState = 1;
            return true;
        } else if (!mBluetoothGatt.connect()) {
            return false;
        } else {
            mConnectionState = 1;
            return true;
        }
    }

    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            mBluetoothGatt.disconnect();
        }
    }

    @SuppressLint({"NewApi"})
    public void close(BluetoothGatt gatt) {
        gatt.disconnect();
        gatt.close();
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothAdapter = null;
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    public void onDestroy() {
        super.onDestroy();
        disconnect();
        close(mBluetoothGatt);
    }
}

package com.example.admin.blenew.adapter;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.admin.blenew.R;
import com.example.admin.blenew.bletester.DeviceConnect;
import com.example.admin.blenew.bletester.MainActivity;
import com.example.admin.blenew.utils.AppData;
import com.example.admin.blenew.utils.ProgressD;

import java.util.ArrayList;

public class BleDeviceListAdapter2 extends RecyclerView.Adapter<BleDeviceListAdapter2.MyViewHolder> {

    private MainActivity mcontext;
    private ArrayList<Integer> RSSIs = new ArrayList();
    private ArrayList<BluetoothDevice> mLeDevices = new ArrayList();
    private ArrayList<String> scanRecords = new ArrayList();

    public BleDeviceListAdapter2(MainActivity mainActivity) {
        mcontext = mainActivity;
    }

    public BleDeviceListAdapter2() {
    }


    public void addDevice(BluetoothDevice device, int RSSI, String scanRecord) {
        if (mLeDevices.contains(device)) {
            for (int i = 0; i < mLeDevices.size(); i++) {
                if (device.getAddress().equals(((BluetoothDevice) mLeDevices.get(i)).getAddress())) {
                    RSSIs.set(i, RSSI);
                    scanRecords.set(i, scanRecord);
                }
            }
            return;
        }
        mLeDevices.add(device);
        RSSIs.add(RSSI);
        scanRecords.add(scanRecord);
        notifyDataSetChanged();
    }

    public BluetoothDevice getDevice(int position) {
        return (BluetoothDevice) mLeDevices.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout content_layout;
        TextView deviceAddress;
        TextView deviceRSSI;
        TextView devicename;
        TextView devicerecord;
        TextView devicerecord_name;

        public MyViewHolder(View view) {
            super(view);
            content_layout = (LinearLayout) view.findViewById(R.id.content_layout);
            devicename = (TextView) view.findViewById(R.id.tv_devicelist_name);
            deviceAddress = (TextView) view.findViewById(R.id.tv_devicelist_address);
            deviceRSSI = (TextView) view.findViewById(R.id.tv_devicelist_rssi);
            devicerecord = (TextView) view.findViewById(R.id.tv_devicelist_scanRecord);
            devicerecord_name = (TextView) view.findViewById(R.id.tv_devicelist_scanRecord_name);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_devicelist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        String name = ((BluetoothDevice) mLeDevices.get(position)).getName();


        if (name != null) {
            holder.devicename.setText(name);
            holder.devicename.setVisibility(View.VISIBLE);

            if (holder.devicename.getText().toString().trim().equals(AppData.getInstance().getLockName())) {

                AppData.getInstance().setLockName(null);
                MainActivity.btn_open.setVisibility(View.GONE);
                ProgressD progressD = ProgressD.show(mcontext, "Connecting", false);
                BluetoothDevice device = getDevice(position);
                AppData.getInstance().setDeviceName(device.getName());
                AppData.getInstance().setDeviceAddress(device.getAddress());

                Fragment newFragment = new DeviceConnect(progressD);
                android.support.v4.app.FragmentTransaction transaction = mcontext.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.device_lauout, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

        } else {
            holder.devicename.setText("Unknown Device");
            holder.devicename.setVisibility(View.GONE);
        }
       /* holder.deviceAddress.setText("Address： " + ((BluetoothDevice) mLeDevices.get(position)).getAddress());
        holder.deviceRSSI.setText("Signal： " + ((Integer) RSSIs.get(position)).toString());
        holder.devicerecord.setText("Broadcast package： \n" + ((String) scanRecords.get(position)));*/
    }

    @Override
    public int getItemCount() {
        return mLeDevices.size();
    }

    public void clear() {
        mLeDevices.clear();
        RSSIs.clear();
        scanRecords.clear();
        notifyDataSetChanged();
    }

}
package com.example.admin.blenew.utils;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

//    @POST("login.php")
//    @FormUrlEncoded
//    Call<LoginResponse> getCode(@Field("user_id") String user_id,
//                                @Field("password") String password,
//                                @Field("barcode") String barcode,
//                                @Field("date_time") String date_time,
//                                @Field("timezone") String timezone);
//
    @POST("submit_log.php")
    @FormUrlEncoded
    Call<SubmiLogResponse> submitLog(@Field("user_id") String user_id,
                                     @Field("scanner_id") String password,
                                     @Field("role_id") String role_id,
                                     @Field("bar_code") String bar_code,
                                     @Field("success") String success,
                                     @Field("closed") String fail,
                                     @Field("battery") String battery_level,
                                     @Field("date_time") String date_time,
                                     @Field("timezone") String timezone);



    @GET("login.php")
    Call<LoginResponse> getCode(@Query("user_id") String user_id,
                                @Query("password") String password,
                                @Query("barcode") String barcode,
                                @Query("date_time") String date_time,
                                @Query("timezone") String timezone);


}

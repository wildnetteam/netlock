package com.example.admin.blenew.bletester;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.example.admin.blenew.R;
import com.example.admin.blenew.adapter.BleDeviceListAdapter2;
import com.example.admin.blenew.service.BleService;
import com.example.admin.blenew.utils.ApiClient;
import com.example.admin.blenew.utils.ApiInterface;
import com.example.admin.blenew.utils.AppData;
import com.example.admin.blenew.utils.LoginPreferences;
import com.example.admin.blenew.utils.LoginResponse;
import com.example.admin.blenew.utils.ProgressD;
import com.example.admin.blenew.utils.SubmiLogResponse;
import com.example.admin.blenew.utils.Utils;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Build.VERSION_CODES.M;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    boolean isExit;
    RecyclerView listView;
    BleDeviceListAdapter2 mBleDeviceListAdapter;
    BluetoothAdapter mBluetoothAdapter;
    private LeScanCallback mLeScanCallback;
    public static RelativeLayout device_lauout;
    LinearLayout login_layout;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String MY_PREFS_NAME = "MY_PREFS_NAME";
    private Context context;
    private TextView et_LockCode, et_barcode;
    private EditText et_userName, et_Password, et_Address;
    private ProgressD progressD;
    private LinearLayout lock_code_layout, address_layout;
    private String userName;
    private String password;
    private static String barcode;
    private static String scanner_device_id;
    public static Button btn_open;
    private CheckBox check;
    private boolean isChecked = false;
    private static String userid,roleId;
    public static MainActivity activity;
    private boolean isBLEON = false;
    private TimeZone timezone;
    public static boolean isScanButtonClick= false;
    public BleService bleService = new BleService();
    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        activity = MainActivity.this;
        try {
            initLoginView();
            initPinCockView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (LoginPreferences.getActiveInstance(context).getRole().equals("1")) {
            setLockCodeName(LoginPreferences.getActiveInstance(context).getLOCK_CODE(), LoginPreferences.getActiveInstance(context).getLOCK_NAME());
            btn_open.setVisibility(View.VISIBLE);
            pin_lock_layout.setVisibility(View.GONE);
            login_layout.setVisibility(View.GONE);
        }
    }

    private RelativeLayout pin_lock_layout;
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private TextView enter_pin_txt, error_txt;
    private String pinFirst, pinSecond;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.e(TAG, "Pin complete: " + pin);

            if (LoginPreferences.getActiveInstance(context).getPIN_LOCK().equals("")) {

                if (enter_pin_txt.getText().toString().equals("Enter Pin")) {
                    pinFirst = pin;
                    mPinLockView.resetPinLockView();
                    enter_pin_txt.setText("Confirm Pin");
                } else {
                    if (pin.equals(pinFirst)) {
                        error_txt.setText("");
                        mPinLockView.resetPinLockView();
                        LoginPreferences.getActiveInstance(context).setPIN_LOCK(pin);
                        pin_lock_layout.setVisibility(View.GONE);
                    } else {
                        mPinLockView.resetPinLockView();
                        error_txt.setText("Pin Not Matched ! Try Again");
                        enter_pin_txt.setText("Enter Pin");
                    }
                    pinSecond = pin;
                }
            } else {
                if (pin.equals(LoginPreferences.getActiveInstance(context).getPIN_LOCK())) {
                    pin_lock_layout.setVisibility(View.GONE);
                } else {
                    mPinLockView.resetPinLockView();
                    error_txt.setText("Pin Not Matched ! Try Again");
                }
            }

        }

        @Override
        public void onEmpty() {
            Log.e(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.e(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String batteryLevel = intent.getStringExtra("battery");
            Log.d("receiver", "Got message: " + batteryLevel);
        }
    };
    private void initPinCockView() {
        pin_lock_layout = findViewById(R.id.pin_lock_layout);
        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        enter_pin_txt = findViewById(R.id.enter_pin_txt);
        error_txt = findViewById(R.id.error_txt);
        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);
        //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
        //mPinLockView.enableLayoutShuffling();
        mPinLockView.setPinLength(4);
        mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.white));
        mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);
    }

    private void initDeviceListView() {
        /* btn_open.setText("Open Lock");*/
        btn_open.setVisibility(View.GONE);
        device_lauout.setVisibility(View.INVISIBLE);
        checkPermissions();
        init();
        startScan();
    }

    private void initLoginView() {
        scanner_device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        /* scanner_device_id = UUID.randomUUID().toString();*/
        login_layout = findViewById(R.id.login_layout);
        check = findViewById(R.id.check);
        lock_code_layout = findViewById(R.id.lock_code_layout);
        address_layout = findViewById(R.id.address_layout);
        et_barcode = findViewById(R.id.et_barcode);
        et_userName = findViewById(R.id.et_userName);
        et_Password = findViewById(R.id.et_Password);
        et_LockCode = findViewById(R.id.et_LockCode);
        et_Address = findViewById(R.id.et_Address);
        btn_open = findViewById(R.id.btn_open);
        device_lauout = findViewById(R.id.device_lauout);
        String userName = LoginPreferences.getActiveInstance(context).getUSERNAME();
        String password = LoginPreferences.getActiveInstance(context).getPASSWORD();
        /*String barcode = settings.getString("barcode", "");*/

       /* if (!TextUtils.isEmpty(barcode)) {
            et_barcode.setText(barcode);
        }*/

        if (!TextUtils.isEmpty(userName)) {
            et_userName.setText(userName);
        }

        if (!TextUtils.isEmpty(password)) {
            isChecked = true;
            check.setChecked(true);
            et_Password.setText(password);
        }

        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isCheckeds) {
                isChecked = isCheckeds;
            }
        });
    }

    public void onScanCall(View view) {
        startActivity(new Intent(context, QrCodeScannerActivity.class));
    }

    public static void callSubmitLogApi(String roleId,String success,String failed) {
        Long tsLong = System.currentTimeMillis()/1000;
        String date_time = tsLong.toString();
        TimeZone timezone = TimeZone.getDefault();
        String name =timezone.getID();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SubmiLogResponse> call = apiInterface.submitLog(userid, scanner_device_id, roleId ,LoginPreferences.getActiveInstance(activity).getBAR_CODE(),success,failed,"error",date_time,name);
        call.enqueue(new Callback<SubmiLogResponse>() {
            @Override
            public void onResponse(Call<SubmiLogResponse> call, Response<SubmiLogResponse> response) {
                Log.e("SubmitLogApiResponse :", "response" + "-" + response);
                Log.e("SubmitLogApiResponse :", "response" + "-" + response.body().message);
                Log.e("SubmitLogApiResponse :", "BAR_CODE" + "-" + LoginPreferences.getActiveInstance(activity).getBAR_CODE());
                Log.e("SubmitLogApiResponse :", "ScannerID" + "-" + scanner_device_id);
                Toast.makeText(activity, response.body().message, Toast.LENGTH_SHORT).show();
                MainActivity.activity.finishAffinity();

            }

            @Override
            public void onFailure(Call<SubmiLogResponse> call, Throwable t) {
                Log.e(TAG, "onFailure---" + t.getMessage());
                Toast.makeText(activity, "Log Not Submitted ! Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                MainActivity.activity.finishAffinity();
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onRestart() {
        if (AppData.getInstance().getQrResult() != null) {
            et_barcode.setText(AppData.getInstance().getQrResult());
            AppData.getInstance().setQrResult(null);
            ongetCodeClick(et_barcode);

        }
        super.onRestart();
    }

    public void ongetCodeClick(View view) {
        userName = et_userName.getText().toString().trim();
        password = et_Password.getText().toString().trim();
        barcode = et_barcode.getText().toString().trim();

        if (TextUtils.isEmpty(userName)) {
            et_userName.setError("This Field Required...");
            et_userName.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            et_Password.setError("This Field Required...");
            et_Password.requestFocus();
        } else if (TextUtils.isEmpty(barcode)) {
            et_barcode.setError("This Field Required...");
            et_barcode.requestFocus();
        } else {
            saveCredetentials(userName, password, barcode);
            callgetCodeApi();
        }
    }

    private void saveCredetentials(String userName, String password, String barcode) {
        if (isChecked) {
            LoginPreferences.getActiveInstance(context).setUSERNAME(userName);
            LoginPreferences.getActiveInstance(context).setPASSWORD(password);
        } else {
            LoginPreferences.getActiveInstance(context).setUSERNAME("");
            LoginPreferences.getActiveInstance(context).setPASSWORD("");
        }
    }

    private void callgetCodeApi() {
        Long tsLong = System.currentTimeMillis()/1000;
        String date_time = tsLong.toString();
        timezone = TimeZone.getDefault();
        String name =timezone.getID();
        Log.e("aakriti", "date time" + date_time );
        Log.e("aakriti", "TimeZone" + name );
        progressD = ProgressD.show(context, "Please Wait...", true);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse> call = apiInterface.getCode(userName, password, barcode,date_time,name);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressD.dismiss();
                Log.e("LoginApiResponse :", "response" + "-" + response.body());
                if (response.isSuccessful()) {
                    if (response.body().status.equals("success")) {
                        LoginPreferences.getActiveInstance(context).setBAR_CODE(et_barcode.getText().toString());
                        LoginResponse.Data data = response.body().data;
                        userid = data.userid;
                        roleId = data.role;
                        String LockCode = data.lockcode;

                        if (isNumeric(LockCode)) {
                            callSubmitLogApi(roleId,"0","1");
                            et_LockCode.setText(LockCode);
                            et_Address.setText(data.userAddress);
                            lock_code_layout.setVisibility(View.VISIBLE);
                            LoginPreferences.getActiveInstance(context).setRole("");
                        } else {
                            LoginPreferences.getActiveInstance(context).setRole(data.role);
                            LoginPreferences.getActiveInstance(context).setLOCK_CODE(data.lockcode);
                            // LoginPreferences.getActiveInstance(context).setLOCK_CODE("F56000005FB4");
                            LoginPreferences.getActiveInstance(context).setLOCK_NAME(data.lock_serial);
                            setLockCodeName(data.lockcode, data.lock_serial);
                            btn_open.setVisibility(View.GONE);
                            SearchDevices1(et_LockCode);
                        }
                    } else {
                        Toast.makeText(context, response.body().message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Server Error...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressD.dismiss();
                Log.e(TAG, "onFailure---" + t.getMessage());
                Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setLockCodeName(String lockcode, String lockName) {
        AppData.getInstance().setLockCode(lockcode);
        AppData.getInstance().setLockName(lockName);
        Log.e("lockcode", lockcode);
        Log.e("lockName", lockName);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    public void SearchDevices(View view) {
        isBLEON = true;
        isScanButtonClick = true;
        btn_open.setEnabled(true);
        lock_code_layout.setVisibility(View.VISIBLE);
        et_LockCode.setText("");
        progressD = ProgressD.show(context, "Searching", false);
        initDeviceListView();
    }

    public void SearchDevices1(View view) {
        isBLEON = true;
        btn_open.setEnabled(true);
        lock_code_layout.setVisibility(View.VISIBLE);
        et_LockCode.setText("");
        progressD = ProgressD.show(context, "Searching", false);
        initDeviceListView();
    }


    private void startScan() {
        getBleAdapter();
        getScanResualt();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    mBluetoothAdapter.startLeScan(mLeScanCallback);
                }
            }
        };
        thread.start();
    }



    @Override
    protected void onPause() {
        setFocusListener();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    private void setFocusListener() {
        et_userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard(v);
            }
        });
        et_Password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard(v);
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void init() {
        listView = (RecyclerView) findViewById(R.id.lv_deviceList);
        mBleDeviceListAdapter = new BleDeviceListAdapter2(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(mBleDeviceListAdapter);
        //  setListItemListener();
    }

    @SuppressLint({"NewApi", "WrongConstant"})
    private void getBleAdapter() {
        mBluetoothAdapter = ((BluetoothManager) getSystemService("bluetooth")).getAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
    }

    @SuppressLint({"NewApi"})
    private void getScanResualt() {
        mLeScanCallback = new LeScanCallback() {
            public void onLeScan(@NonNull final BluetoothDevice device, @NonNull final int rssi, @NonNull final byte[] scanRecord) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            if(progressD != null)
                                progressD.dismiss();
                            mBleDeviceListAdapter.addDevice(device, rssi, Utils.bytesToHex(scanRecord));
                            invalidateOptionsMenu();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    @SuppressLint({"NewApi"})
    protected void onResume() {
        // setFocusListener();
        if (btn_open.isEnabled()) {
            startScan();
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter());
        super.onResume();
    }

    @SuppressLint({"NewApi"})
    protected void onDestroy() {
        super.onDestroy();
        if (isBLEON) {
            CallDestroy();
        }
    }

    public void CallDestroy() {
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mBluetoothAdapter.cancelDiscovery();
            mBleDeviceListAdapter.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            // exitBy2Click();
            finish();
            //  onBackPressed();
        }
        return false;
    }

    @SuppressLint("WrongConstant")
    private void exitBy2Click() {
        if (isExit) {
            onDestroy();
            finish();
            System.exit(0);
            return;
        }
        isExit = true;
        Toast.makeText(this, "Press again to exit", 0).show();
        new Timer().schedule(new TimerTask() {
            public void run() {
                isExit = false;
            }
        }, 2000);
    }

    @SuppressLint("NewApi")
    private void checkPermissions() {
        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (Build.VERSION.SDK_INT >= M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            } else {
                OPENFPS();
            }
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    OPENFPS();
                    Log.e("onRequestPermissionsResult :--", "Coarse location permission granted");
                } else {
                    Toast.makeText(context, "Please Enable GPS from Settings..", Toast.LENGTH_SHORT).show();
                    checkPermissions();
                }
            }
        }
    }

    private void OPENFPS() {
        if (!isLocationServicesAvailable(context)) {
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    public static boolean isLocationServicesAvailable(Context context) {
        int locationMode = 0;
        String locationProviders;
        boolean isAvailable = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            isAvailable = (locationMode != Settings.Secure.LOCATION_MODE_OFF);
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            isAvailable = !TextUtils.isEmpty(locationProviders);
        }

        boolean coarsePermissionCheck = (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        boolean finePermissionCheck = (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

        return isAvailable && (coarsePermissionCheck || finePermissionCheck);
    }

}
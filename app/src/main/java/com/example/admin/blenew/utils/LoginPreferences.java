package com.example.admin.blenew.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class LoginPreferences {
    private static LoginPreferences preferences = null;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private Context context;


    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    public void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public static LoginPreferences getActiveInstance(Context context) {
        if (preferences == null) {
            preferences = new LoginPreferences(context);
        }
        return preferences;
    }

    private String Role = "Role";
    private String USERNAME = "USERNAME";
    private String PASSWORD = "PASSWORD";
    private String LOCK_CODE = "LOCK_CODE";
    private String LOCK_NAME = "LOCK_NAME";
    private String PIN_LOCK = "PIN_LOCK";
    private String BAR_CODE = "BAR_CODE";

    public String getBAR_CODE() {
        return mPreferences.getString(this.BAR_CODE, "");
    }

    public void setBAR_CODE(String BAR_CODE) {
        editor = mPreferences.edit();
        editor.putString(this.BAR_CODE, BAR_CODE);
        editor.commit();
    }

    public String getRole() {
        return mPreferences.getString(this.Role, "");
    }

    public void setRole(String role) {
        editor = mPreferences.edit();
        editor.putString(this.Role, role);
        editor.commit();
    }

    public String getPIN_LOCK() {
        return mPreferences.getString(this.PIN_LOCK, "");
    }

    public void setPIN_LOCK(String PIN_LOCK) {
        editor = mPreferences.edit();
        editor.putString(this.PIN_LOCK, PIN_LOCK);
        editor.commit();
    }

    public String getUSERNAME() {
        return mPreferences.getString(this.USERNAME, "");
    }

    public void setUSERNAME(String USERNAME) {
        editor = mPreferences.edit();
        editor.putString(this.USERNAME, USERNAME);
        editor.commit();
    }

    public String getPASSWORD() {
        return mPreferences.getString(this.PASSWORD, "");
    }

    public void setPASSWORD(String PASSWORD) {
        editor = mPreferences.edit();
        editor.putString(this.PASSWORD, PASSWORD);
        editor.commit();
    }

    public String getLOCK_CODE() {
        return mPreferences.getString(this.LOCK_CODE, "");
    }

    public void setLOCK_CODE(String LOCK_CODE) {
        editor = mPreferences.edit();
        editor.putString(this.LOCK_CODE, LOCK_CODE);
        editor.commit();
    }

    public String getLOCK_NAME() {
        return mPreferences.getString(this.LOCK_NAME, "");
    }

    public void setLOCK_NAME(String LOCK_NAME) {
        editor = mPreferences.edit();
        editor.putString(this.LOCK_NAME, LOCK_NAME);
        editor.commit();
    }

    public void clearAll() {
        LoginPreferences.getActiveInstance(context).setRole("");
    }
}